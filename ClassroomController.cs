using Google.Apis.Auth.OAuth2;
using Google.Apis.Classroom.v1;
using Google.Apis.Classroom.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.Apis.Requests;
using Google;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;
using System.Net;

namespace TodoApi.Controllers
{
    [Route("api/classrooms")]
    [ApiController]
    public class ClassroomController : ControllerBase
    {
        //The services and credential access may need to move elsewhere, but this accesses the API for now
        static string[] Scopes = { ClassroomService.Scope.ClassroomCoursesReadonly, ClassroomService.Scope.ClassroomCourseworkMeReadonly };
        static string ApplicationName = "Classroom API .NET Quickstart";
        UserCredential credential;
        ClassroomService service;
        
        public ClassroomController() {
            //Initialize file stream and API service connection
            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }
            service = new ClassroomService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
        }

        //*** GET Requests ***//
        [HttpGet]
        public async Task<ActionResult<List<Course>>> getClassrooms() {
            string pageToken = null;
            var courses = new List<Course>();

            do
            {
                var request = service.Courses.List();
                request.PageSize = 100;
                request.PageToken = pageToken;
                var response = request.Execute();
                courses.AddRange(response.Courses);
                pageToken = response.NextPageToken;
            } while (pageToken != null);

            if (courses.Count == 0)
            {
                Console.WriteLine("No courses found.");
                return null;
            }
            else
            {
                Console.WriteLine("Courses:");
                foreach (var course in courses)
                {
                    Console.WriteLine("{0} ({1})\n", course.Name, course.Id);
                }
                return courses;
            }
        }

        [HttpGet("{id}")]        
        public async Task<ActionResult<Course>> getCourse(long id) {
            string courseId = id.ToString();
            Course course = null;
            try
            {
                course = service.Courses.Get(courseId).Execute();
                Console.WriteLine("Course '{0}' found\n.", course.Name);
                return course;
            }
            catch (GoogleApiException e)
            {
                if (e.HttpStatusCode == HttpStatusCode.NotFound)
                {
                    Console.WriteLine("Course with ID '{0}' not found.\n", courseId);
                    return null;
                }
                else
                {
                    throw e;
                }
            }
        }

        //Retrieves all submissions as a student
        [HttpGet("{courseId}/studentsubmissions")]
        public async Task<ActionResult<ListStudentSubmissionsResponse>> getStudentSubmissions(long courseId) {
            string course_id = courseId.ToString();
            var request = service.Courses.CourseWork.StudentSubmissions.List(
                course_id,
                // "-" symbol retrieves all coursework versus a specific coursework by id (eg. "4524211" vs "-")
                "-");
            ListStudentSubmissionsResponse response = request.Execute();
            if (response.StudentSubmissions != null && response.StudentSubmissions.Count > 0) {
                foreach (var submission in response.StudentSubmissions)
                {
                    Console.WriteLine("{0} {1}", submission.Id, submission.State);
                }
                return response;
            }
            else
            {
                Console.WriteLine("No submissions found");
                return null;
            }
        }
        
        //Should retrieve all coursework of a given course
        //returns 403 without proper authorization, likely via Google Administrations
        [HttpGet("{courseId}/coursework")]
        public async Task<ActionResult<List<CourseWork>>> getCourseworkList(long courseId) {
            string pageToken = null;
            var courseWorkList = new List<CourseWork>();

            Console.WriteLine("{0} coursework check", courseId);
            do
            {
                var request = service.Courses.CourseWork.List(courseId.ToString());
                request.PageSize = 100;
                request.PageToken = pageToken;
                var response = request.Execute();
                courseWorkList.AddRange(response.CourseWork);
                pageToken = response.NextPageToken;
            } while (pageToken != null);
            
            if (courseWorkList.Count == 0)
            {
                Console.WriteLine("No Course Work found");
                return null;
            }
            else 
            {
                foreach (var courseWork in courseWorkList)
                {
                    Console.WriteLine("{0} {1} {2}\n", courseWork.Id, courseWork.Title, courseWork.CourseId);
                }
                return courseWorkList;
            }
        }


        // POST Requests

        //returns 403 without proper authorization, likely via Google Administrations
        [HttpPost]
        public async Task<ActionResult<Course>> createCourse() {
            var course = new Course
            {
                Name = "10th Grade Biology",
                Section = "Period 2",
                DescriptionHeading = "Welcome to 10th Grade Biology",
                Description = "We'll be learning about about the structure of living creatures "
                    + "from a combination of textbooks, guest lectures, and lab work. Expect "
                    + "to be excited!",
                Room = "301",
                OwnerId = "me",
                CourseState = "PROVISIONED"
            };

            try {
                course = service.Courses.Create(course).Execute();
            }
            catch (GoogleApiException e) {
                Console.WriteLine(e);
                throw e;
            }
            Console.WriteLine("Course created: {0} ({1})", course.Name, course.Id);
            return course;
        }

        //returns 403 without proper authorization, likely via Google Administrations
        [HttpPost("{courseId}/coursework")]
        public async Task<ActionResult<CourseWork>> createCourseWork(long courseId) {
            Console.WriteLine(courseId);
            var coursework = new CourseWork
            {
                Title = "Ant colonies",
                Description = "Read the article about ant colonies and complete the quiz.",
                Materials = new Material[] {
                    new Material {Link = {Url = "http://example.com/ant-colonies"}},
                    new Material {Link = {Url = "http://example.com/ant-quiz"}}
                },
                WorkType = "ASSIGNMENT",
                State = "PUBLISHED",
            };

            try
            {
                coursework = service.Courses.CourseWork.Create(
                    coursework,
                    courseId.ToString()
                ).Execute();
            }
            catch (GoogleApiException e)
            {
                Console.WriteLine(e);
                throw e;
            }
            Console.WriteLine("Coursework created");
            return coursework;
        }
    }
}