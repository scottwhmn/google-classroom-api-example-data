# README #

This repo for holding example JSON data for Google Classroom API.

You will need the following to use the Google API in the controller file:
OAuth Client ID as a "credentials.json" file

The following guide will have links for getting the credentials
https://developers.google.com/classroom/quickstart/dotnet

### Setting localhost

Running through .NET will require a URI redirects to be authorized.  Since .NET Core will likely randomize the port number when connecting, Google API may not accept it.

The url will look like http://localhost:8080/authorize or http://127.0.0.1:52314/authorize
Fortunately you can simply set the URI redirect as http://localhost/authorize or http://127.0.0.1/authorize

### Google Classroom API Doc Link

https://developers.google.com/classroom/guides/manage-courses
There are also other examples in the sidebar, though some only python examples